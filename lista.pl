num_elementos([],0).
num_elementos([Cabeca|Cauda], N):-
	num_elementos(Cauda, NCauda),
	N is 1 + NCauda.
%Cacula o Somatorio dos elementos de uma Lista
somatorio([], 0).
somatorio([Cabeca|Cauda], Soma):-
	somatorio(Cauda, SomaCauda),
	Soma is Cabeca + SomaCauda.

%Calcula a Media dos Elementos de uma Lista
media([],0).
media(Lista,Media):-
	 num_elementos(Lista, N),
	 somatorio(Lista,S),
	 Media is S/N.

%Concatena duas Lista
concatenadas([],L,L).
concatenadas([Cabeca|Cauda1],L2, [Cabeca|Cauda2]):-
	concatenadas(Cauda1, L2, Cauda2).

%Verifica se um elemento pertence a lista
pertence_a(X,[X|Cauda]).
pertence_a(X,[_|Cauda]):-
	pertence_a(X,Cauda).



%Atividade para o Lar
%listificada([a,b,c,d],L).
%L= [[a],[b],[c],[d]]
listificada([], []).
listificada([Cab|Cauda], [[Cab]|Cauda2]):-
	listificada(Cauda,Cauda2).


%Lista_dobro([2,4,6],L).
%L = [4,8,12].



%num_ocorrencias(a, [a,menina, azul],N).
%N = 1.
num_ocorrencias(Palavra, [],0).

num_ocorrencias(Palavra, [Palavra|Cauda], C):-
	num_ocorrencias(Palavra, Cauda, C2),
	C is 1 + C2.

num_ocorrencias(Palavra, [Cabeca|Cauda],C):-
	num_ocorrencias(Palavra, Cauda, C).



%todas_antes(casaco,[o,meu,casaco,azul],L).
%L = [o, meu].
todas_antes(Palavra,Lista, []) :-
	not(pertente_a(Palavra,Lista)).

todas_antes(Palavra, [], []).
todas_antes(Palavra, [Palavra| _], []).
todas_antes(Palavra, [Cabeca, Cauda1], [Cabeca|Cauda2]):-
	todas_antes(Palavra, Cauda1, Cauda2).



%trabalho 1
%elementos_repetidos({a,a,b], Z).
%Z=[a,b]
elementos_repetidos([], []).
elementos_repetidos([Cabeca|Cauda], [Cabeca|Cauda2]):-
       pertence_a(Cabeca, Cauda),
       elementos_repetidos(Cauda,Cauda2),
       not(pertence_a(Cabeca, Cauda2)).
elementos_repetidos([Cabeca|Cauda], L):-
	elementos_repetidos(Cauda,L).


%intercalado([a,b],[1,2,3,4],Z).
%Z = [a,1,b,2,3,4]?
%2 EXERCICIO
intercalado([], [], []).
intercalado([], X, X).
intercalado(Y, [], Y).
intercalado([Cabeca1|Cauda1], [Cabeca2|Cauda2], [Cabeca1, Cabeca2|Z]):-
	intercalado(Cauda1, Cauda2, Z).


%insercao_orde(6,[3,4,5,8,9], Z).
%Z=[3,4,5,6,8,9].
%3 EXERCICIO
insercao_orde(X, [], [X]).
insercao_orde(X, [Cabeca1|Cauda1], [Cabeca1|Sublista]):-
	X > Cabeca1,
	insercao_orde(X, Cauda1, Sublista).
insercao_orde(X, Lista, [X|Sublista]):-
	insercao_orde(Lista, [], Sublista).

%ordenada([10,11,9], Z).
%Z=[9,10,11].
%4 EXERCICIO
ordenada([], []).
ordenada([Cab1 | Cauda], L):-
	ordenada(Cauda, L),
	insercao_orde(Cab1, Cauda, L).

%EXERC�CIO 8: Escreva um predicado que dado um n�mero, retorne uma lista com os
%seus fatores primos:

%&Example:
%?- fatores(315, L).
%L = [3,3,5,7]

%fatores(n, L):-
%f	n = 2.
%ffatores(
%ffatores _aux(n, d, [d|Cauda])
%f    eh_primo(d),
%f    n% d = 0.
 %f   fatores_aux(n, d-1, Cauda).
%verificar fatores caso o numero nao seja primo


%EXERC�CIO 5: Escreva um predicado que linearize uma sequencia de listas
%encadeadas.

%fExemplo:
%?- lineariza([a, [b, [c, d], e]], X).
%X = [a, b, c, d, e]


empacota([[A,B] | C1], [[A] | C2]):-
   A \= B,
   empacota([B,C1], C2).
empacota([A,A|C1], [[A|C2]|C3]):-
  empacota([A|C1], [C2,C3]).


%

ultimo(X, [X]).
ultimo(X, [_|Cauda]):-
	ultimo(X, Cauda),
	!.




















